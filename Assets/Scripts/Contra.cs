﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Contra : MonoBehaviour
{
	#region Public Config Variable
	public ContraCallback Callback = null;
	#endregion

	#region Private Dataholder Variables
	private KeyCode[] contra = new KeyCode[] { KeyCode.UpArrow, KeyCode.UpArrow, KeyCode.DownArrow, KeyCode.DownArrow,
		KeyCode.LeftArrow, KeyCode.RightArrow, KeyCode.LeftArrow, KeyCode.RightArrow, KeyCode.B, KeyCode.A, KeyCode.Return};
	private int index = 0;
	#endregion

	#region Frame Update
	// Update is called once per frame
	void Update()
	{
		if (Input.anyKeyDown)
		{
			if (Input.GetKeyDown(contra[index++]))
			{
				// Hit, proceed
				if ((index >= contra.Length) && (Callback != null))
				{
					Callback();
					index = 0;
				}
			}
			else
			{
				index = 0;
			}
		}
	}
	#endregion

	#region Delegate Declaration
	/// <summary>
	/// Delegate method type for contra callback method
	/// </summary>
	public delegate void ContraCallback();
	#endregion
}