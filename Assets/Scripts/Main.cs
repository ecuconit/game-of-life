﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Main : MonoBehaviour
{
	#region Public Config Variable
	[Tooltip("Initial size of the map")]
	public Vector2Int size = new Vector2Int(10, 10);
	[Tooltip("Clock speed multiplier")]
	[Range(0.0f, 10.0f)]
	public float multiplier = 1.0f;

	[Tooltip("Place marker prefab")]
	public GameObject marker;

	[Tooltip("Any living cell within this range (inclusive) will stay alive")]
	public Vector2Int liveRange = new Vector2Int(2, 3);
	[Tooltip("Any dead cell within this range (inclusive) will reproduce")]
	public Vector2Int reproduceRange = new Vector2Int(3, 3);

	[Tooltip("On randomization, the probability of living cell")]
	[Range(0.0f, 1.0f)]
	public float randomHit = 0.45f;
	#endregion

	#region Private Data Holder Variable
	private bool initialized = true;
	private Vector2Int mapSize = Vector2Int.zero;

	// Camera control
	private Camera main;

	// Map cache
	private bool[,] data = null;
	private Marker[,] cache = null;

	// Timer
	private bool paused = false;
	private float accu = 0.0f;
	private const float threshold = 1.0f;
	#endregion

	#region Contructor
	/// <summary>
	/// Object create event
	/// </summary>
	void Awake()
	{
		// Verify references
		if (size.x < 5 || size.y < 5)
		{
			Global.ToDebug("Unable to start the game with below minimum [Size] less than (5, 5).", true);
			initialized = false;
		}
		if (multiplier < 0.0f)
		{
			Global.ToDebug("Speed [Multiplier] cannot be less than 0.", true);
			initialized = false;
		}
		if (marker == null)
		{
			Global.ToDebug("Need a refrence to position [Marker] prefab.", true);
			initialized = false;
		}
		if ((liveRange == null) || (liveRange.x >liveRange.y) || (liveRange.x < 1) || (liveRange.y > 8))
		{
			Global.ToDebug("The [Living Range] value needs to be between 1 ~ 8.", true);
			initialized = false;
		}
		if ((reproduceRange == null) || (reproduceRange.x > reproduceRange.y) || (reproduceRange.x < 1) || (reproduceRange.y > 8))
		{
			Global.ToDebug("The [Reproduce Range] value needs to be between 1 ~ 8.", true);
			initialized = false;
		}
	}

	/// <summary>
	/// Object start event
	/// </summary>
	void Start()
	{
		// Verification
		if ((!initialized) || (!Initialization()))
		{
			Global.ToDebug("Game cannot start because it cannot be initialized correctly.", true);
			this.enabled = false;
		}

		List<string> temp = new List<string>();
	}

	/// <summary>
	/// Initialzaing current scene
	/// </summary>
	/// <returns>Whether or not initialization was completed successfully</returns>
	public bool Initialization()
	{
		// Initializing grid
		mapSize = size;
		data = new bool[mapSize.x + 2, mapSize.y + 2];
		cache = new Marker[mapSize.x, mapSize.y];
		LocationCallback callback = LocationClicked;

		Vector2 offset = (mapSize - Vector2.one) / 2;
		GameObject temp = null;
		for (int x = 0; x < mapSize.x; x++)
		{
			for (int y = 0; y < mapSize.y; y++)
			{
				// Instantiate a marker
				temp = GameObject.Instantiate(marker, new Vector3(x - offset.x, 0, y - offset.y), Quaternion.identity);
				temp.name = string.Format("Marker {0}-{1}", x, y);
				data[x + 1, y + 1] = false;
				cache[x, y] = temp.GetComponent<Marker>();
				cache[x, y].Callback = callback;
				cache[x, y].Location = new Vector2Int(x, y);
			}
		}

		// Associate Contra handler
		Contra contra = gameObject.GetComponent<Contra>();
		if (contra != null)
			contra.Callback = RandomizeMap;

		return true;
	}
	#endregion

	#region Frame Update
	/// <summary>
	/// On frame update
	/// </summary>
	void Update()
	{
		// Simple verification
		if (paused)
			return;

		// Check for update event
		accu += (Time.deltaTime * multiplier);
		if (accu > threshold)
		{
			Generation();
			accu -= threshold;
		}
	}

	/// <summary>
	/// Update the generation of cells
	/// </summary>
	private void Generation()
	{
		// Lock timer
		paused = true;

		// Calculate map, no need to initialize
		int[,] sum = new int[mapSize.x + 2, mapSize.y + 2];
		for (int x = 1; x <= mapSize.x; ++x)
		{
			for (int y = 1; y <= mapSize.y; ++y)
			{
				// Check status
				if (data[x, y])
				{
					sum[x + 1, y + 1]++;
					sum[x, y + 1]++;
					sum[x - 1, y + 1]++;
					sum[x + 1, y]++;
					sum[x - 1, y]++;
					sum[x + 1, y - 1]++;
					sum[x, y - 1]++;
					sum[x - 1, y - 1]++;
				}
			}
		}
		// Update alive status
		for (int x = 1; x <= mapSize.x; ++x)
		{
			for (int y = 1; y <= mapSize.y; ++y)
			{
				if (data[x, y])
				{
					// If cell is alive, check for isolation and overcrowding
					if ((sum[x, y] < liveRange.x) || (sum[x, y] > liveRange.y))
						data[x, y] = cache[x - 1, y - 1].Alive = false;					
				}
				else
				{
					// If cell is dead, check for reproduction
					if ((sum[x, y] >= reproduceRange.x) && (sum[x, y] <= reproduceRange.y))
						data[x, y] = cache[x - 1, y - 1].Alive = true;
				}
			}
		}

		// Release timer
		paused = false;
	}
	#endregion

	#region Helper Method
	/// <summary>
	/// Dump current map content
	/// </summary>
	/// <param name="name"></param>
	/// <param name="map"></param>
	/// <param name="size"></param>
	private void DumpMap(string name, bool[,] map, Vector2Int size)
	{
		// Dump
		string temp = null;
		Debug.Log(name);
		System.Diagnostics.Debug.WriteLine(name);
		for (int y = mapSize.y; y >= 1; --y)
		{
			temp = string.Empty;
			for (int x = 1; x <= mapSize.x; ++x)
			{
				temp += ("[" + (data[x, y] ? "1" : "0") + "]");
			}
			Debug.Log(temp);
			System.Diagnostics.Debug.WriteLine(temp);
		}
		Debug.Log("");
		System.Diagnostics.Debug.WriteLine("");
	}

	/// <summary>
	/// On location click, toggle its alive status
	/// </summary>
	/// <param name="loc">Which location was clicked</param>
	public void LocationClicked(Vector2Int loc)
	{
		// Toggle data
		cache[loc.x, loc.y].Alive = data[loc.x + 1, loc.y + 1] = !data[loc.x + 1, loc.y + 1];

		// Trigger turn based game
		// Generation();
	}

	/// <summary>
	/// Randomize current map
	/// </summary>
	public void RandomizeMap ()
	{
		// Lock timer
		paused = true;

		// Random set values
		System.Random rand = new System.Random();
		for (int x = 0; x < mapSize.x; x++)
		{
			for (int y = 0; y < mapSize.y; y++)
			{
				data[x + 1, y + 1] = cache[x, y].Alive = (rand.NextDouble() > randomHit);
			}
		}

		// Release timer
		paused = false;
	}
	#endregion

	#region Delegate Declaration
	/// <summary>
	/// Delegate method type for location callback
	/// </summary>
	/// <param name="loc">What location is calling</param>
	public delegate void LocationCallback(Vector2Int loc);
	#endregion
}