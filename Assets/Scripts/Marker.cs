﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Marker : MonoBehaviour
{
	#region Public Config Variable
	public Vector2Int Location = Vector2Int.zero;
	public GameObject Cell;
	public Main.LocationCallback Callback = null;
	#endregion

	#region Private Data Holder Variable
	private bool alive = false;
	#endregion

	#region Getter & Setter
	/// <summary>
	/// Get or set current mark's alive status
	/// </summary>
	public bool Alive
	{
		get { return alive; }
		set
		{
			// Toggle UI if necessary
			if (alive != value)
				Cell.SetActive(alive = value);
		}
	}
	#endregion

	#region Public Interface Method
	/// <summary>
	/// Mouse click
	/// </summary>
	void OnMouseUpAsButton()
	{
		// Update callback
		if (Callback != null)
			Callback(Location);
		else
			Global.ToDebug("Click callback not initialized", true);
	}
	#endregion
}